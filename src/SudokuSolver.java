import java.util.stream.IntStream;

public class SudokuSolver {

	private static final int EMPTY_FIELD_VALUE = 0;

	public boolean solveBoard(SudokuBoard board) {
		int fields[][] = board.fields;
		int rowCount = fields.length;

		for (int rowIndex = 0; rowIndex < rowCount; rowIndex++) {
			int row[] = fields[rowIndex];
			int fieldCount = row.length;

			for (int fieldIndex = 0; fieldIndex < fieldCount; fieldIndex++) {
				int field = row[fieldIndex];

				if (field == EMPTY_FIELD_VALUE) {
					for (int numberToTest = 1; numberToTest <= 9; numberToTest++) {
						fields[rowIndex][fieldIndex] = numberToTest;

						if (isValid(board, rowIndex, fieldIndex) && solveBoard(board)) {
							return true;
						}

						fields[rowIndex][fieldIndex] = EMPTY_FIELD_VALUE;
					}

					return false;
				}
			}
		}


		return true;
	}

	private boolean isValid(SudokuBoard board, int rowIndex, int columnIndex) {

		if (!isRowConstraintValid(board, rowIndex)) {
			return false;
		}

		if (!isColumnConstraintValid(board, columnIndex)) {
			return false;
		}

		if (!isSectionConstraintValid(board, rowIndex, columnIndex)) {
			return false;
		}

		return true;
	}

	public boolean isRowConstraintValid(SudokuBoard board, int rowIndex) {
		int rowCount = board.fields.length;
		boolean validFields[] = new boolean[rowCount];

		return IntStream.range(0, rowCount)
			.allMatch(columnIndex -> isConstraintValid(board, rowIndex, validFields, columnIndex));
	}

	public boolean isColumnConstraintValid(SudokuBoard board, int columnIndex) {
		int columnCount = board.fields[0].length;
		boolean validFields[] = new boolean[columnCount];

		return IntStream.range(0, columnCount)
				.allMatch(rowIndex -> isConstraintValid(board, rowIndex, validFields, columnIndex));
	}

	public boolean isSectionConstraintValid(SudokuBoard board, int rowIndex, int columnIndex) {
		boolean validFields[] = new boolean[board.fields.length];

		int sectionRowStart = rowIndex;
		int sectionRowEnd   = sectionRowStart + 2; // TODO: base '2' on variable section size

		int sectionColumnStart = columnIndex;
		int sectionColumnEnd   = sectionColumnStart + 2;

		// NOTE: Clean-up & check for row
		if (sectionColumnEnd > board.fields.length) {
			sectionColumnEnd = board.fields.length;
		}

		for (int r = sectionRowStart; r < sectionRowEnd; r++) {
			for (int c = sectionColumnStart; c < sectionColumnEnd; c++) {
				if (!isConstraintValid(board, r, validFields, c)) {
					return false;
				}
			}
		}

		return true;
	}

	public boolean isConstraintValid(SudokuBoard board, int rowIndex, boolean validFields[], int columnIndex) {
		int fieldValue = board.fields[rowIndex][columnIndex];

		if (fieldValue == 0) {
			return true;
		}

		if (!validFields[fieldValue - 1]) {
			validFields[fieldValue - 1] = true;
			return true;
		}

		return false;
	}

}
