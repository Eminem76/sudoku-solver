
public class SudokuBoard {
	
	protected int fields[][];
	
	public void setFields(int fields[][]) {
		this.fields = fields;
	}

	public void print() {
		System.out.println("+ - - - + - - - + - - - +");
		
		int rowCount = this.fields.length;
		
		for (int rowIndex = 0; rowIndex < rowCount; rowIndex++) {
			printFieldsInRow(rowIndex);
			
			System.out.println();
			
			// if (rowIndex == 2 || rowIndex == 5 || rowindex == 8) {
			if (rowIndex % 3 == 2) {
				System.out.println("+ - - - + - - - + - - - +");
			}
		}
	}

	private void printFieldsInRow(int rowIndex) {
		
		System.out.print("| ");
		
		int row[] = fields[rowIndex];
		
		int fieldCount = row.length;
		
		for (int fieldIndex = 0; fieldIndex < fieldCount; fieldIndex++) {		
			int field = row[fieldIndex];
			
			if (field == 0) {
				System.out.print(". ");
			} else {
				System.out.print(field + " ");
			}
			
			// if (fieldIndex == 2 || fieldIndex == 5 || rowindex == 8) {
			if (fieldIndex % 3 == 2) {
				System.out.print("| ");
			}
		}
	}

}
