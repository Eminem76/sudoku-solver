public class Main {

	public static void main(String[] args) {

		SudokuBoard unsolvedBoard = new SudokuBoard();

		unsolvedBoard.setFields(new int[][] {
			{ 8, 0, 0,   0, 0, 0,   0, 0, 0 },
			{ 0, 0, 3,   6, 0, 0,   0, 0, 0 },
			{ 0, 7, 0,   0, 9, 0,   2, 0, 0 },

			{ 0, 5, 0,   0, 0, 7,   0, 0, 0 },
			{ 0, 0, 0,   0, 4, 5,   7, 0, 0 },
			{ 0, 0, 0,   1, 0, 0,   0, 3, 0 },

			{ 0, 0, 1,   0, 0, 0,   0, 6, 8 },
			{ 0, 0, 8,   5, 0, 0,   0, 1, 0 },
			{ 0, 9, 0,   0, 0, 0,   4, 0, 0 },
		});

		SudokuSolver sudokuSolver = new SudokuSolver();
		sudokuSolver.solveBoard(unsolvedBoard);
//		SudokuBoard solvedBoard = sudokuSolver.solveBoard(unsolvedBoard);
//
//		System.out.println("=== Unsolved Board ===");
//		unsolvedBoard.print();
//
//		System.out.println("=== Solved Board ===");
//		solvedBoard.print();

	}

}
